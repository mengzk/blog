# 创建数据库

- CREATE DATABASE 数据库名;
- drop database <数据库名>;



### 删除，添加或修改表字段

- 使用 `ALTER TABLE <表名> DROP <列名>` 来删除表中 `指定列`
``` sql
ALTER TABLE users DROP tag;
``` 

> 注：如果数据表中只剩余一个字段则无法使用 `DROP` 来删除字段。

- 使用 `ALTER TABLE <表名> ADD <列名> <字段类型>` 向表中添加`列`
``` sql
ALTER TABLE users ADD tag INT;
```

- 如果需要指定新增字段的位置，可以使用关键字 `FIRST (设定为第一列),AFTER 字段名（设定于某个字段之后）`
``` sql
ALTER TABLE users ADD tag INT FIRST;
ALTER TABLE users ADD test INT AFTER id;
```

- 修改字段类型及名称

可以在`ALTER`命令中使用 `MODIFY` 或 `CHANGE` 子句

1. MODIFY: 使用
``` sql
/** 字段 tag 的类型从 CHAR(1) 改为 CHAR(10) */
ALTER TABLE users MODIFY tag CHAR(10);
```

2. CHANG: 使用
``` sql
/** tag 类型从 CHAR 改为 BIGINT,名称由 tag 改为 t */
ALTER TABLE users CHANGE tag t BIGINT;
```

- ALTER TABLE 对 Null 值和默认值的影响

> 如果不设置默认值，会自动设置该字段默认为 NULL。

``` sql
ALTER TABLE users MODIFY tga BIGINT NOT NULL DEFAULT 100;
```

- 修改表名
``` sql
ALTER TABLE users RENAME TO user;
```

- 其他
``` sql
/** 修改存储引擎：修改为myisam */
alter table tableName engine=myisam;

/** 删除外键约束：keyName是外键别名 */
alter table tableName drop foreign key keyName;

/** 修改字段相对位置：name1为要修改的字段，type1为该字段原来类型，first和after二选一, first放在第一位，after放在name2字段后面 */
alter table tableName modify name1 type1 first|after name2;
```