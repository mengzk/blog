SELECT语句控制您要查看的列和行.

SELECT语句 语法:
```sql
SELECT
    column_1, column_2, ...
FROM
    table_1
[INNER | LEFT |RIGHT] JOIN table_2 ON conditions
WHERE
    conditions
GROUP BY column_1
HAVING group_conditions
ORDER BY column_1
LIMIT offset, length; 
```

下面是SELECT的使用：

- SELECT 后跟逗号分隔列或星号（*）列表，表示返回所有列。
- FROM 指定要查询数据的表或视图。
- JOIN 根据特定的连接条件从其他表中获取相关数据。
- WHERE 子句过滤结果集中的行。
- GROUP BY  子句将一组行分组到组中，并在每个组上应用聚合函数。
- HAVING  子句根据GROUP BY子句定义的组过滤组。
- ORDER BY  子句指定用于排序的列的列表。
- LIMIT 约束返回的行数。
在查询语句中要求必须包含SELECT和FROM ,其它都是可选项。


SELECT语句允许您通过在SELECT子句中指定逗号分隔列的列表来查询表的部分数据。
```sql
SELECT lastname, firstname, jobtitle FROM employees; 
```
即使employees表有很多列，SELECT语句只返回上述三列数据

想获取所有列，可以列出所有列名或使用星号（*）表示获取表中所有列数据
```sql
SELECT * FROM employees; 
```


