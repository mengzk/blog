## DISTINCT子句
查询数据时，可能会出现重复的行。删除重复的行，可以在SELECT子句中使用 DISTINCT 语句。

DISTINCT 子句语法：
```sql
SELECT DISTINCT columns FROM table_name
WHERE where_conditions; 
```

### DISTINCT 和 NULL值
如果去重列字段中有 NULL 值，则MySQL仅保留一个NULL值，因为DISTINCT 将所有 NULL当作相同的值。

### DISTINCT 多列
DISTINCT 子句 可以包含多个列。MySQL会使用这些列中的值组合来确定是否重复。

```sql
SELECT DISTINCT state,city FROM customers 
WHERE state IS NOT NULL 
ORDER BY state,city; 
```

### DISTINCT 与 GROUP BY 语句
不使用聚合函数时在SELECT语句中使用GROUP BY子句，则GROUP BY子句的行为与DISTINCT 子句类似。

```sql
SELECT state FROM customers
GROUP BY state; 
```

一般来说，DISTINCT 语句是GROUP BY语句的特殊情况。
将ORDER BY子句添加到DISTINCT子句的语句中，与使用GROUP BY子句的语句返回的结果集相同。
```sql
SELECT DISTINCT state FROM customers 
ORDER BY state; 
```

### DISTINCT和聚合函数
可以将DISTINCT子句与聚合函数（例如，SUM，AVG和COUNT ）一起使用，将聚合函数应用于结果集之前删除重复的行。

```sql
SELECT COUNT(DISTINCT state) FROM customers 
WHERE country = 'CN'; 
```

### DISTINCT 和 LIMIT子句
如果将DISTINCT 子句与LIMIT子句一起使用，MySQL会在找到LIMIT子句中指定的唯一行数时立即停止搜索。

```sql
SELECT DISTINCT state FROM customers 
WHERE state IS NOT NULL LIMIT 5; 
```