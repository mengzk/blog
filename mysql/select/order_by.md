# ORDER BY子句
对结果集进行排序。

可用操作：
- 按单列或多列对结果集进行排序。
- 按升序或降序对不同列的结果集进行排序。

ORDER BY语法：
```sql
SELECT column1, column2,... FROM table
ORDER BY column1 [ASC|DESC], column2 [ASC|DESC],... 
```
ASC-升序和DESC-降序。默认按升序排序。


### ORDER BY按表达式排序
ORDER BY 子句还允许您根据表达式对结果集进行排序。
```sql
SELECT number, ordercode, quantity * price FROM orderdetail
ORDER BY number, ordercode, quantity * price;
// 别名-可以使用列别名, 按列别名排序
SELECT number, ordercode, quantity * price as amount FROM orderdetail
ORDER BY number, ordercode, amount;
 ```

### ORDER BY 自定义排序
ORDER BY 子句可以使用FIELD()函数为列中的值自定义排序顺序。

如果要基于以下状态对订单排序：
```
Pay
Fail
Cancel
Finish
```
可以使用FIELD函数将这些值映射到数值列表，并使用其排序;

请参阅以下查询：
```sql
SELECT orderNumber, status FROM orders
ORDER BY FIELD(status, 'Pay', 'Fail', 'Cancel', 'Finish');
```

