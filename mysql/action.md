# 语法

# 查询

### 查询语句: `SELECT * FROM 表名`

`SELECT` 表示执行一个查询，`*` 表示“所有列”，`FROM` 表示从哪个表查询 

``` sql
SELECT * FROM user;
```

# 删除数据

DELETE语句的基本语法是：

> DELETE FROM <表名> WHERE ...;

``` sql

DELETE FROM students WHERE id=1;
```

- DELETE语句的WHERE条件也是用来筛选需要删除的行，因此和UPDATE类似，DELETE语句也可以一次删除多条记录;
- 如果WHERE条件没有匹配到任何记录，DELETE语句不会报错，也不会有任何记录被删除。
- 要特别小心的是，不带WHERE条件的DELETE语句会删除整个表的数据;
- DELETE语句也会返回删除的行数以及WHERE条件匹配的行数。

``` sql
DELETE FROM students WHERE id>=5 AND id<=7;

/** 注意：整个表的所有记录都会被删除 */
DELETE FROM students;
```

## 小优化

### 1.插入或替换
如果我们希望插入一条新记录（INSERT），但如果记录已经存在，就先删除原记录，再插入新记录。此时，可以使用REPLACE语句，这样就不必先查询，再决定是否先删除再插入：
``` sql
REPLACE INTO students (id, class_id, name, gender, score) VALUES (1, 1, '小明', 'F', 99);
```

### 2.插入或更新
如果我们希望插入一条新记录（INSERT），但如果记录已经存在，就更新该记录，此时，可以使用INSERT INTO ... ON DUPLICATE KEY UPDATE ...语句：

``` sql
INSERT INTO students (id, class_id, name, gender, score) VALUES (1, 1, '小明', 'F', 99) ON DUPLICATE KEY UPDATE name='小明', gender='F', score=99;
```

### 3.插入或忽略
如果我们希望插入一条新记录（INSERT），但如果记录已经存在，就啥事也不干直接忽略，此时，可以使用INSERT IGNORE INTO ...语句：

INSERT IGNORE INTO students (id, class_id, name, gender, score) VALUES (1, 1, '小明', 'F', 99);

### 4.快照
如果想要对一个表进行快照，即复制一份当前表的数据到一个新表，新创建的表结构和SELECT使用的表结构完全一致。
可以结合CREATE TABLE和SELECT：
``` sql
-- 对class_id=1的记录进行快照，并存储为新表students_of_class1:
CREATE TABLE students_of_class1 SELECT * FROM students WHERE class_id=1;
```

### 5.强制使用指定索引
在查询的时候，数据库系统会自动分析查询语句，并选择一个最合适的索引。但是很多时候，数据库系统的查询优化器并不一定总是能使用最优索引。如果我们知道如何选择索引，可以使用FORCE INDEX强制查询使用指定的索引。例如：
``` sql
/** 指定索引的前提是索引idx_class_id必须存在。 */
SELECT * FROM students FORCE INDEX (idx_class_id) WHERE class_id = 1 ORDER BY id DESC;
```

### 6.写入查询结果集
如果查询结果集需要写入到表中，可以结合INSERT和SELECT，将SELECT语句的结果集直接插入到指定表中。

例如，创建一个统计成绩的表statistics，记录各班的平均成绩：
``` sql
CREATE TABLE statistics (
    id BIGINT NOT NULL AUTO_INCREMENT,
    class_id BIGINT NOT NULL,
    average DOUBLE NOT NULL,
    PRIMARY KEY (id)
);
```
然后，我们就可以用一条语句写入各班的平均成绩：
``` sql
INSERT INTO statistics (class_id, average) SELECT class_id, AVG(score) FROM students GROUP BY class_id;
```
确保INSERT语句的列和SELECT语句的列能一一对应，就可以在statistics表中直接保存查询的结果


# 数据插入
INSERT语句的基本语法是：

> INSERT INTO <表名> (字段1, 字段2, ...) VALUES (值1, 值2, ...);

``` sql
INSERT INTO students (class_id, name, gender, score) VALUES (2, '大牛', 'M', 80);
/** 可以一次添加多条，只需在VALUES中指定多个记录，由(...)包含的一组值 */
INSERT INTO students (class_id, name, gender, score) VALUES
  (1, '大宝', 'M', 87),
  (2, '二宝', 'M', 81);
```

注意到我们并没有id字段，也没有列出id字段对应的值，这是因为id字段是一个自增主键，它的值可以由数据库自己推算出来。此外，如果一个字段有默认值，那么在INSERT语句中也可以不出现。

注意，字段顺序`不必`和数据库表的`字段顺序一致`，但`值`的顺序`必须`和`字段顺序一致`。
也就是说，可以写`INSERT INTO students (score, gender, name, class_id)` ...，但是对应的VALUES就得变成`(80, 'M', '大牛', 2)`。


# 数据更新

UPDATE语句的基本语法是：

> UPDATE <表名> SET 字段1=值1, 字段2=值2, ... WHERE ...;

``` sql
/** 更新id=1的信息 */
UPDATE students SET name='张三', score=66 WHERE id=1;

UPDATE students SET name='未知', score=77 WHERE id BETTWEEN 20 AND 27;

UPDATE students SET score=score+10 WHERE score<80;
```

- 注: `UPDATE`语句的`WHERE`条件和`SELECT`语句的`WHERE`条件是一样的。

- 注: 如果`WHERE`条件没有匹配到任何记录，`UPDATE`语句不会报错，也不会有任何记录被更新。

- UPDATE语句会返回更新的行数以及WHERE条件匹配的行数。

### `UPDATE`语句可以没有`WHERE`条件

``` sql
/** 这个语句会把整个表的所有记录都更新 */
UPDATE students SET score=60;
```

# 数据插入
INSERT语句的基本语法是：

> INSERT INTO <表名> (字段1, 字段2, ...) VALUES (值1, 值2, ...);

``` sql
INSERT INTO students (class_id, name, gender, score) VALUES (2, '大牛', 'M', 80);
/** 可以一次添加多条，只需在VALUES中指定多个记录，由(...)包含的一组值 */
INSERT INTO students (class_id, name, gender, score) VALUES
  (1, '大宝', 'M', 87),
  (2, '二宝', 'M', 81);
```

注意到我们并没有id字段，也没有列出id字段对应的值，这是因为id字段是一个自增主键，它的值可以由数据库自己推算出来。此外，如果一个字段有默认值，那么在INSERT语句中也可以不出现。

注意，字段顺序`不必`和数据库表的`字段顺序一致`，但`值`的顺序`必须`和`字段顺序一致`。
也就是说，可以写`INSERT INTO students (score, gender, name, class_id)` ...，但是对应的VALUES就得变成`(80, 'M', '大牛', 2)`。


## 小优化

### 1.插入或替换
如果我们希望插入一条新记录（INSERT），但如果记录已经存在，就先删除原记录，再插入新记录。此时，可以使用REPLACE语句，这样就不必先查询，再决定是否先删除再插入：
``` sql
REPLACE INTO students (id, class_id, name, gender, score) VALUES (1, 1, '小明', 'F', 99);
```

### 2.插入或更新
如果我们希望插入一条新记录（INSERT），但如果记录已经存在，就更新该记录，此时，可以使用INSERT INTO ... ON DUPLICATE KEY UPDATE ...语句：

``` sql
INSERT INTO students (id, class_id, name, gender, score) VALUES (1, 1, '小明', 'F', 99) ON DUPLICATE KEY UPDATE name='小明', gender='F', score=99;
```

### 3.插入或忽略
如果我们希望插入一条新记录（INSERT），但如果记录已经存在，就啥事也不干直接忽略，此时，可以使用INSERT IGNORE INTO ...语句：

INSERT IGNORE INTO students (id, class_id, name, gender, score) VALUES (1, 1, '小明', 'F', 99);

### 4.快照
如果想要对一个表进行快照，即复制一份当前表的数据到一个新表，新创建的表结构和SELECT使用的表结构完全一致。
可以结合CREATE TABLE和SELECT：
``` sql
-- 对class_id=1的记录进行快照，并存储为新表students_of_class1:
CREATE TABLE students_of_class1 SELECT * FROM students WHERE class_id=1;
```

### 5.强制使用指定索引
在查询的时候，数据库系统会自动分析查询语句，并选择一个最合适的索引。但是很多时候，数据库系统的查询优化器并不一定总是能使用最优索引。如果我们知道如何选择索引，可以使用FORCE INDEX强制查询使用指定的索引。例如：
``` sql
/** 指定索引的前提是索引idx_class_id必须存在。 */
SELECT * FROM students FORCE INDEX (idx_class_id) WHERE class_id = 1 ORDER BY id DESC;
```

### 6.写入查询结果集
如果查询结果集需要写入到表中，可以结合INSERT和SELECT，将SELECT语句的结果集直接插入到指定表中。

例如，创建一个统计成绩的表statistics，记录各班的平均成绩：
``` sql
CREATE TABLE statistics (
    id BIGINT NOT NULL AUTO_INCREMENT,
    class_id BIGINT NOT NULL,
    average DOUBLE NOT NULL,
    PRIMARY KEY (id)
);
```
然后，我们就可以用一条语句写入各班的平均成绩：
``` sql
INSERT INTO statistics (class_id, average) SELECT class_id, AVG(score) FROM students GROUP BY class_id;
```
确保INSERT语句的列和SELECT语句的列能一一对应，就可以在statistics表中直接保存查询的结果

