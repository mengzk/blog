# MySQL事务声明
MySQL为我们提供了以下控制事务的重要声明：

- 要启动事务，请使用START TRANSACTION  语句。也可以使用START TRANSACTION的别名BEGIN或  BEGIN WORK
- 要提交当前事务并使其永久更改，请使用COMMIT语句。
- 要回滚当前事务并取消其更改，请使用ROLLBACK语句。
- 要禁用或启用当前事务的自动提交模式，请使用SET autocommit语句。
默认情况下，MySQL会自动将更改永久提交到数据库。

要强制MySQL不自动提交更改：
```sql
SET autocommit = 0; 
/** 或者 */
SET autocommit = OFF 

/** 您可以使用以下语句显式启用自动提交模式： */

SET autocommit = 1; 
/** 或者 */
SET autocommit = ON; 
```

1. **`COMMIT` 示例**

为了使用事务，首先必须将SQL语句分解为逻辑部分，并确定何时应提交或回滚数据。

以下说明了创建新销售订单的步骤：

- 使用 START TRANSACTION 语句启动事务。
- 从orders表中选择最新的销售订单编号，并使用下一个销售订单编号作为新的销售订单编号。
在orders表格中插入新的销售订单。
- 将销售订单项插入 orderdetails 表中。
- 使用COMMIT语句提交事务。

（可选）您可以从两个表orders和orderdetails表中选择数据以检查新的销售订单

```sql
-- 1. 开始一个新手事物
START TRANSACTION;
 
-- 2. 获取最新的订单编号
SELECT @orderNum:=MAX(orderNum)+1
FROM orders;
 
-- 3. 向145客户插入新订单编号
INSERT INTO orders(orderNum,
                   orderDate,
                   requiredDate,
                   shippedDate,
                   status,
                   customerNum)
VALUES(@orderNum,
       '2005-05-31',
       '2005-06-10',
       '2005-06-11',
       'In Process', 145);
        
-- 4. 插入订单详情
INSERT INTO orderdetails(orderNum,
                         productCode,
                         quantity,
                         price,
                         orderLineNum)
VALUES(@orderNum,'S18_1749', 30, '136', 1),
      (@orderNum,'S18_2248', 50, '55.09', 2); 
      
-- 5. 提交事实
COMMIT; 
-- 结果：1 row in set (0.02 sec)
```
要获取新创建的销售订单：
```sql
SELECT 
    a.orderNum,
    orderDate,
    requiredDate,
    shippedDate,
    status,
    comments,
    customerNum,
    orderLineNum,
    productCode,
    quantity,
    price
FROM orders a
INNER JOIN orderdetails b USING (orderNum)
WHERE a.orderNum = 1026;

-- 结果：2 rows in set (0.01 sec)
```

2. **`ROLLBACK` 实例**

首先，登录MySQL数据库服务器并从订单表中删除数据：

```sql

START TRANSACTION;
/** 删除orders表所有数据 */
DELETE FROM orders;

/** 再在单独的会话中登录MySQL数据库服务器 并查询 orders表数据 */
SELECT COUNT(*) FROM orders;

在第二个会话中，我们仍然可以看到orders表中的数据。

第一个会话中做了删除，但这些不是永久性的。
在第一个会话中，可以提交或回滚更改。
ROLLBACK; 
-- 结果：Query OK, 0 rows affected (0.04 sec)

/** 再在第一个会话中查询 */
SELECT COUNT(*) FROM orders;
-- 结果：删除的数据已恢复
```







