# MySQL临时表

在MySQL中，临时表是一种特殊类型的表，允许您存储临时结果集，您可以在单个会话中多次重复使用。

当不能使用带有JOIN子句的单个SELECT语句或非常耗时，临时表非常方便。
它可以使用临时表来存储当前结果，并使用另一个查询来处理它。

临时表功能：

- 使用 CREATE TEMPORARY TABLE 创建临时表。
- 当会话结束或连接终止时，MySQL会自动删除临时表。可以用 DROP TABLE 显式删除临时表。
- 临时表仅可供创建它的客户端访问。不同客户端创建相同名称的临时表不会报错，因为只有创建临时表的客户端才能看到它。在同一会话中，临时表名称不能相同。
- 临时表可以与数据库中的普通表具有相同的名称。


### 创建MySQL临时表
要创建临时表，只需将 TEMPORARY 关键字添加到CREATE TABLE语句中即可。

实例：
```sql
CREATE TEMPORARY TABLE top10
SELECT p.code, c.name, ROUND(SUM(p.count),2) sales
FROM payments p
INNER JOIN hots c ON c.code = p.code
GROUP BY p.code
ORDER BY sales DESC
LIMIT 10; 
```
现在，您可以从top10customers临时表中查询数据就像从普通表中查询：
```sql
SELECT code, name, sales
FROM top10 ORDER BY sales; 
```

### 删除MySQL临时表
使用 DROP TABLE 语句删除临时表，但最好添加 TEMPORARY 关键字

语句：
```sql
DROP TEMPORARY TABLE table_name; 
DROP TEMPORARY TABLE  
```
- 此语句仅能删除临时表，而不是普通表。

实例：
```sql
DROP TEMPORARY TABLE top10; 
```
> 注意：使用DROP TEMPORARY TABLE 语句删除普通表，将会报错，因为普通表不能作为临时表