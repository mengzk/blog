### UNIQUE约束简介

有时希望在列中强制实施唯一性值.

如供应商表中供应商的电话必须是唯一的，或者供应商名称和地址的组合不得重复。

要强制执行此规则，需要使用UNIQUE约束。

UNIQUE约束是列约束或表约束，它定义了将列或列组中的值约束为唯一的规则。

语法：
```
CREATE TABLE table_1(
    column_name_1  data_type UNIQUE,
); 
```
或者将UNIQUE约束定义为表约束：
```
CREATE TABLE table_1(
   ...
   column_name_1 data_type,
   ...
   UNIQUE(column_name_1)
); 
```
如果插入或更新column_name_1列时出现重复的值，将发出错并拒绝更改。

如果要跨列强制实施唯一值，必须将UNIQUE约束定义为表约束，并用逗号分隔：
```
CREATE TABLE table_1(
   ...
   column_name_1 data_type,
   column_name_2 data type,
   ...
   UNIQUE(column_name_1,column_name_2)
); 
```
使用两个值column_name_1和column_name_2列中的值的组合来评估唯一性。

如果要为 UNIQUE约束 指定特定名称，使用 CONSTRAINT子句：
```
CREATE TABLE table_1(
   ...
   column_name_1 data_type,
   column_name_2 data type,
   ...
   CONSTRAINT constraint_name UNIQUE(column_name_1,column_name_2)
); 
```


##### 管理MySQL UNIQUE约束
向表中添加唯一约束时，MySQL会为数据库创建相应的BTREE索引。

以下SHOW INDEX语句显示在supplier表上创建的所有索引。
```
SHOW INDEX FROM mysqldemo.suppliers;
```

有两个BTREE索引对应于创建的两个UNIQUE约束。

要删除UNIQUE约束，可以使用 DROP INDEX 或 ALTER TABLE语句：
```
DROP INDEX index_name ON table_name; 
ALTER TABLE table_name
DROP INDEX index_name; 
```

要删除suppliers表上的uc_name_address约束，请使用以下语句：
```
DROP INDEX uc_name_address ON suppliers; 
```

再次执行SHOW INDEX语句以验证uc_name_unique是否已删除约束。
```
SHOW INDEX FROM mysqldemo.suppliers; 
```

##### 如果要将UNIQUE约束添加到已存在的表中？

先使用以下ALTER TABLE语句：
```
ALTER TABLE table_name
ADD CONSTRAINT constraint_name UNIQUE (column_list); 
```
例如，要将uc_name_addressUNIQUE约束添加回suppliers表，请使用以下语句：
```
ALTER TABLE suppliers
ADD CONSTRAINT uc_name_address UNIQUE (name,address); 
```
