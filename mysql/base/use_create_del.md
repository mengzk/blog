# USE

USE关键字后面是您要选择的数据库的名称，选择特定数据库。

USE 语句：
```sql
USE database_name; 
```

通过命令行选择MySQL数据库

1. 首先，使用指定用户登录MySQL，例如root：
```
mysql -u root -p
```
使用-u 指定用户root，然后使用-p标志。

2. 用 USE命令来选择数据库，例如mysqldemo：
```
mysql> use mysqldemo;
Database changed
```
如果数据库存在，MySQL发出消息“ Database changed”。

如果数据库不存在，MySQL会报错：
```
mysql> use abc; 
ERROR 1049 (42000): Unknown database 'abc'
```

3. 您可以使用以下database()函数获取当前连接的数据库的名称：
```
mysql> select database();
```

4. 要选择数据库
```
mysql> use testdb;
Database changed
```


### 创建数据库

使用数据库前需要创建一个数据库。数据库是数据的容器。它存储您想存储的任何类型的数据。

在MySQL中，数据库是一组对象，用于存储和操作表，数据库视图，触发器和 存储过程等数据。

创建数据库语句：
```
CREATE DATABASE [IF NOT EXISTS] database_name; 
```
- CREATE DATABASE 语句后跟是您要创建的数据库名称。
- IF NOT EXISTS  是可选子句。
- IF NOT EXISTS 子句可防止您创建数据库服务器中已存在的新数据库时出错。

例如，要创建demo数据库，可以CREATE DATABASE 按如下方式执行语句：
```sql
CREATE DATABASE demo; 
```

### 显示数据库

SHOW DATABASES 语句列出了MySQL数据库服务器中的所有数据库。
```sql
SHOW DATABASES; 
```
1. 选择数据库
```sql
USE database_name; 
```


### 删除数据库

删除数据库意味着永久删除数据库中包含的所有表和数据库本身。

要删除数据库，请使用以下DROP DATABASE 语句：
```
DROP DATABASE [IF EXISTS] database_name; 
/** 在MySQL中，模式是数据库的同义词，因此可以互换使用它们 */
DROP SCHEMA [IF EXISTS] database_name; 
```
- DROP DATABASE 子句后面是要删除的数据库名称。
- 与CREATE DATABASE 语句类似，IF EXISTS 语句的可选部分，用于防止您删除数据库服务器中不存在的数据库。

例子：
```sql
CREATE DATABASE IF NOT EXISTS tempdb; 
SHOW DATABASES;
DROP DATABASE IF EXISTS tempdb; 
```


### 创建表

CREATE TABLE语法

要在数据库中创建新表，使用 CREATE TABLE  语句。

语法:
```sql
CREATE TABLE [IF NOT EXISTS] table_name(column1,column2, ...) ENGINE=storage_engine
```
- 表名在数据库中必须是唯一的。
- IF NOT EXISTS 可选，用于检查创建的表是否已存在，如果已经存在就不会创建新表。
- column1 部分列中为表指定列，列以逗号分隔。
- ENGINE 可以指定存储引擎, 默认使用InnoDB。

在表中定义列语法：
```
column_name data_type(length) [NOT NULL] [DEFAULT value] [AUTO_INCREMENT] COMMENT '这是一个新列'
```
- column_name 指定列的名称。
- data_type 每列都有特定的数据类型和最大长度，例如：VARCHAR(255) 
- NOT NULL 指示列不允许NULL值。
- DEFAULT value 用于指定列的默认值。
- AUTO_INCREMENT 表示只要将新行插入表中，就会自动生成一个叠加值。每个表只有一 AUTO_INCREMENT 列。
- COMMENT 列的注释;

### 实例
```sql
CREATE TABLE IF NOT EXISTS test (
    id INT AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    create_date DATE,
    status INT NOT NULL,
    desc TEXT,
    PRIMARY KEY (id)
)  ENGINE=INNODB; 
```
任务表包含以下列：

- id是一个自动增量列。使用INSERT向表中添加行而未指定id的值，id将以1开头的自动生成的整数，id是主键列。
- name 列是一个可变字符串列，其最大长度为50.
- NOT NULL 表示列必须有一个值,不能为空。
- create_date是接受NULL的日期列。
- status 是不能为NULL的INT列。
- desc 是一个接受NULL的TEXT列。


### 删除表

要删除现有表，使用 DROP TABLE

语法：
```sql
DROP [TEMPORARY] TABLE [IF EXISTS] table_name [, table_name] ...
[RESTRICT | CASCADE] 
```
- DROP TABLE 语句从数据库中永久删除表及其数据。
- 还可以使用 DROP TABLE 语句删除多个表，每个表由逗号（,）分隔。
- TEMPORARY选项允许您仅删除临时表。确保不会意外删除非临时表非常有用。
- IF EXISTS 如果表不存在不会报错。使用 IF EXISTS 时，会生成一个NOTE，可以使用 SHOW WARNING 检索。

DROP TABLE语句仅删除表及其数据。它不会删除与已删除表关联的特定用户权限。<br/>
因此如果在此之后重新创建具有相同名称的表，则现有权限将应用于新表，这可能会带来安全风险。

RESTRICT 和 CASCADE 选项保留给MySQL的未来版本。


##### 实例：
我们将删除上一个教程中使用CREATE TABLE语句教程创建的tasks表。

删除test_table和不存在的表 test_table2 如下：
```sql
DROP TABLE IF EXISTS test_table, test_table2; 
```
如果检查数据库，您将看到tasks表已被删除。
您可以使用以下SHOW WARNING语句检查由于不存在的表而由MySQL生成的NOTE ：
```sql
SHOW WARNINGS; 
```

##### DROP TABLE 基于模式
假设有许多以 test开头的表，希望使用单个 DROP TABLE 语句删除所有表。

MySQL并没有提供 DROP TABLE LIKE 但可以根据模式匹配删除表的语句

首先，声明两个接受数据库模式的变量和一个表匹配的模式：
```sql
-- 设置表的数据库及匹配模式
SET @schema = 'demo';
SET @pattern = 'test%'; 
```
接下来，需要构建一个动态DROP TABLE语句：
```sql
-- 动态构建SQL (DROP TABLE tbl1, tbl2...;)
SELECT CONCAT('DROP TABLE ',GROUP_CONCAT(CONCAT(@schema,'.',table_name)),';')
INTO @droplike
FROM information_schema.tables
WHERE @schema = database()
AND table_name LIKE @pattern; 
```
查询MySQL的数据字典在information_schema表，它包含所有数据库中所有表的数据，
连接数据库@schema（demo ）中与pattern @pattern test%）匹配所有表前缀加上 DROP TABLE 关键字。
GROUP_CONCAT函数创建一个逗号分隔的表列表。

然后，我们可以显示动态SQL以验证它是否正常工作：
```sql
-- 显示构建的SQL脚本
SELECT @droplike; 

-- 执行动态SQL 
PREPARE stmt FROM @dropcmd;
EXECUTE stmt;
DEALLOCATE PREPARE stmt; 
```


### TRUNCATE TABLE

MySQL TRUNCATE TABLE语句允许您删除表中的所有数据。

TRUNCATE TABLE语句类似于没有WHERE子句的DELETE语句。<br/>
在某些情况下，TRUNCATE TABLE 语句比 DELETE 语句更有效。

语法：
```sql
TRUNCATE TABLE table_name; 
```
- TRUNCATE TABLE子句后指定要删除数据的表名。
- TABLE关键字是可选项。注意区分 TRUNCATE TABLE 语句和 TRUNCATE 函数。

如果是InnoDB表，将在删除数据之前检查表中是否存在任何可用的外键约束。

- 如果表有任何外键约束，则 TRUNCATE TABLE 语句将逐个删除行。
- 如果外键约束具有DELETE CASCADE操作，则还会删除子表中的相应行。
- 如果外键约束未指定DELETE CASCADE，则会逐个删除行，当遇到子表中的行引用的行时，将停止并报错。
- 如果表没有任何外键约束，则删除表并重新创建有相同结构的新空表，这比用DELETE更快更有效。
- 如果您正在使用其他存储引擎，则TRUNCATE TABLE语句将丢弃并重新创建一个新表。

> 注意: 如果表有AUTO_INCREMENT列，则TRUNCATE TABLE语句会将自动增量值重置为零。<br/>
TRUNCATE TABLE语句不使用DELETE语句，因此，不会调用与表关联的DELETE触发器。