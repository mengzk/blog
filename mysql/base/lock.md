
# MySQL 表锁定

锁是与表关联的标志。MySQL允许客户端显式获取表锁，以防止其他会话在特定时间段内访问同一个表。<br/>
客户端会话只能为自己获取或释放表锁。它 `无法` 获取或释放 `其他会话` 的表锁。

### LOCK 和 UNLOCK TABLES语法
显式获取表锁：
```
LOCK TABLES table_name [READ | WRITE] 
```
要锁定表，请在 `LOCK TABLES` 关键字后指定 `表名称`。还可指定锁的类型: `READ` 或 `WRITE`。

要释放表的锁 语句：
```
UNLOCK TABLES;
```

1. **读锁**

**`READ` 特点：**

- `READ`可以通过多个会话同时获取表的锁,其他会话可以从表中读取数据而无需获取锁。
- 持有`READ`锁的会话`只能读取`表中的数据而`无法写入`,在释放锁之前，其他会话将无法写入表，而是进入等待状态，直到释放。
- 如果会话正常或异常终止，MySQL将隐式释放所有锁。此功能也与WRITE锁相关。


##### 例子：
使用CONNECTION_ID()函数获取当前连接ID：
```sql
SELECT CONNECTION_ID(); 
/** 对表tb1上锁 */
LOCK TABLE tb1 READ; 
/** 尝试在tb1表中插入一行*/
INSERT INTO tb1(col) VALUES(11); 
/** 运行结果 */
ERROR 1099 (HY000): Table 'tb1' was locked with a READ lock and can't be updated
```
一旦锁定了表，就无法在同一会话中将数据写入表中。

让READ从另一个会话中检查锁定。
```sql
/** 首先，连接并检查连接ID： */
SELECT CONNECTION_ID(); 
/** 查询表tb1 */
SELECT * FROM tb1; 
/** 在tb1表中插入一个新行： */
INSERT INTO tb1(col) VALUES(20);

```
第二个会话的插入操作处于等待状态，因为第一个会话READ已经在tb1表上获取了锁，但尚未释放。

可以从`SHOW PROCESSLIST`查看详细信息
```sql
SHOW PROCESSLIST; 
```
使用`UNLOCK TABLES`语句释放锁定。READ从第一个会话释放锁定后，`INSERT`执行第二个会话中的操作。


2. **写锁**

**`WRITE` 特点：**

- 保存表锁的唯一会话可以从表中读取和写入数据。
- 在`WRITE`锁定释放之前，其他会话`无法` `读取`, `写入`数据到表中。


##### 例子：
```sql
/** 首先获取锁定 */
LOCK TABLE tb1 WRITE; 
/** 然后在tb1表中插入一行 */
INSERT INTO tb1(col) VALUES(11);
/**接下来从tb1查询数据 */
SELECT * FROM tb1;
/** 上面运行正常 */

/** 之后新打开一个会话，尝试写入和读取数据 */
INSERT INTO tb1(col) VALUES(21);
SELECT * FROM tb1; 
/** 上面操作置于等待状态。*/

/** 可以使用SHOW PROCESSLIST进行检查 */
SHOW PROCESSLIST；

/** 最后从第一个会话中释放锁定 */
UNLOCK TABLES; 

/** 第二个会话中的所有待处理操作将都执行 */
```
### 读锁与写锁
- 读锁是“共享”锁，可以防止会话获取写锁，但不能防止其他会话读锁。
- 写锁是“独占”锁，可以防止任何其他类型的锁。


