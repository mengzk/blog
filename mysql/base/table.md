### MySQL序列

序列是按升序生成的整数列表，即1,2,3 ...许多程序用序列生成唯一的数字用于识别，例用户ID。

要想使用 序列，只需 AUTO_INCREMENT 设置为列，通常是主键列。

使用 AUTO_INCREMENT 规则:

- 每个表只有一 AUTO_INCREMENT 列，其数据类型通常是整数。
- AUTO_INCREMENT 列必须编入索引，可以是索引 PRIMARY KEY 或 UNIQUE 索引。
- AUTO_INCREMENT 列必须具有 NOT NULL 约束。AUTO_INCREMENT 设置为列时，会自动将NOT NULL隐式添加。

序列示例:
```sql
CREATE TABLE users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(50),
    last_name VARCHAR(50)
);
```

##### 序列有以下属性：

- AUTO_INCREMENT 列的起始值为1，当向列中插入null或在INSERT语句中省略其值时，它会增加1。
- 获取最后生成的序列号，使用 LAST_INSERT_ID()函数。生成的序列在会话中是唯一的。
- 向表中插入新行并指定序列值，如果序列号不存在，则插入序列号，如果已存在则发出错误。
如果插入值大于表中最大序列号，将使用新值作为起始序列号，这会在序列中产生间隙。
- 使用 UPDATE 语句将 AUTO_INCREMENT 序列值更新为已存在的值，如果列带有唯一索引，将报重复键错误。
- 如果将 AUTO_INCREMENT 列更新为大于列中现有值，将使用新值作为起始序列号。
- 如果 DELETE 语句删除最后插入的行，可能不会重复使用已删除的序列号，具体取决于表的存储引擎。
- 如果删除一行，MyISAM不会重复使用已删除的序列号，InnoDB 也不重用序列号。
- 列设置 AUTO_INCREMENT 后，可以重置自动增量值，例如 ALTER TABLE 语句。


### ALTER TABLE 语句
ALTER TABLE 语句修改现有表的结构。它允许添加列，删除列，更改列的数据类型，添加主键，重命名表等等。

语法:
```sql
ALTER TABLE table_name action1[,action2,…] 
```
- 在ALTER TABLE  子句后指定要更改的表名。
- 应用于表的一组操作。操作可以是任何操作，如添加列，添加主键...。还可以在单语句中使用多操作，用`,`分隔。

实例:
```sql
CREATE TABLE users (
    id INT NOT NULL,
    name VARCHAR(45) NULL,
    create_date DATE NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX id_unique (id ASC)
);
```

##### ALTER TABLE 更改列

1. ALTER TABLE 语句为列设置自动增量属性
```sql
ALTER TABLE users CHANGE COLUMN id id INT(11) NOT NULL AUTO_INCREMENT; 
```

2. ALTER TABLE 添加新列

```sql
ALTER TABLE users ADD COLUMN age INT NULL AFTER name; 
```

3. ALTER TABLE 删除列
```sql
ALTER TABLE users DROP COLUMN create_date; 
```

4. ALTER TABLE 语句重命名表
> 注意: 在重命名表之前，应考虑是否会影响数据库和应用程序层。
```sql
ALTER TABLE users RENAME TO peoples; 
```


### RENAME TABLE
由于业务需求发生变化，需要将当前表重命名为新表以更好地反映新情况。MySQL提供了一个非常有用的语句，可以更改一个或多个表的名称。

语句：
```sql
RENAME TABLE old_table_name TO new_table_name; 
```
> 旧表（old_table_name）必须存在，新表（new_table_name）必须不存在。

除了表之外，我们还可以使用 RENAME TABLE 语句重命名视图。

在执行RENAME TABLE语句之前，我们必须确保没有活动事务或锁定表。

> 注意：不能使用 RENAME TABLE 语句重命名 临时表，但可以使用 ALTER TABLE 语句重命名临时表。

在安全性方面，必须将授予旧表的权限手动迁移到新表。
- 必须手动调整引用表的其他数据库对象，例如视图，存储过程，触发器，外键约束等。



### DROP COLUMN
在某些情况下，需要从表中删除一个或多个列。这种情况下可以使用 ALTER TABLE DROP COLUMN 语句

语法：
```sql
ALTER TABLE table_name DROP COLUMN column1;

/** COLUMN 关键字可以忽略 */
ALTER TABLE table_name DROP column1; 

ALTER TABLE table_name DROP COLUMN column1, DROP COLUMN column2, ...;
```
- 在ALTER TABLE 子句后指定包含要删除的列的表。
- 将列的名称放在 DROP COLUMN 子句之后。
- 关键字 COLUMN 是可选的
- 可以同时删除多列
- 必须在删除列之前删除外键约束。


### ADD COLUMN

要向现有表添加新列，使用ALTER TABLE ADD COLUMN语句

语法；
```sql
ALTER TABLE table_name ADD [COLUMN] column_name column_definition [FIRST|AFTER existing_column];
``` 
- 在ALTER TABLE子句后指定表名。
- 将新列及其定义放在 ADD COLUMN 子句之后。COLUMN 关键字是可省略的。
- 可以指定 FIRST 将新列添加为表的第一列，使用AFTER column1 指定列之后添加新列。默认添加为最后一列。
- 可以向表中添加两个或更多列
```sql
ALTER TABLE table_name
ADD [COLUMN] column_name_1 column_1_definition [FIRST|AFTER existing_column],
ADD [COLUMN] column_name_2 column_2_definition [FIRST|AFTER existing_column], ...; 
```

### TRUNCATE TABLE

MySQL TRUNCATE TABLE语句允许您删除表中的所有数据。

TRUNCATE TABLE语句类似于没有WHERE子句的DELETE语句。<br/>
在某些情况下，TRUNCATE TABLE 语句比 DELETE 语句更有效。

语法：
```sql
TRUNCATE TABLE table_name; 
```
- TRUNCATE TABLE子句后指定要删除数据的表名。
- TABLE关键字是可选项。注意区分 TRUNCATE TABLE 语句和 TRUNCATE 函数。

如果是InnoDB表，将在删除数据之前检查表中是否存在任何可用的外键约束。

- 如果表有任何外键约束，则 TRUNCATE TABLE 语句将逐个删除行。
- 如果外键约束具有DELETE CASCADE操作，则还会删除子表中的相应行。
- 如果外键约束未指定DELETE CASCADE，则会逐个删除行，当遇到子表中的行引用的行时，将停止并报错。
- 如果表没有任何外键约束，则删除表并重新创建有相同结构的新空表，这比用DELETE更快更有效。
- 如果您正在使用其他存储引擎，则TRUNCATE TABLE语句将丢弃并重新创建一个新表。

> 注意: 如果表有AUTO_INCREMENT列，则TRUNCATE TABLE语句会将自动增量值重置为零。<br/>
TRUNCATE TABLE语句不使用DELETE语句，因此，不会调用与表关联的DELETE触发器。