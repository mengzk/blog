### CHECK约束简介

CHECK约束简介
SQL标准提供了CHECK约束，允许在插入和更新之前验证列或列组的数据。

如可以定义CHECK约束以强制零件的成本为正：
```sql
CREATE TABLE IF NOT EXISTS parts (
    part_no VARCHAR(18) PRIMARY KEY,
    description VARCHAR(40),
    cost DECIMAL(10 , 2 ) NOT NULL CHECK(cost > 0),
    price DECIMAL (10,2) NOT NULL
); 
```
SQL标准还允许您将多个CHECK约束应用于多个列的列或code CHECK约束。

如要确保价格始终大于或等于成本，请使用以下CHECK约束：
```sql
CREATE TABLE IF NOT EXISTS parts (
    part_no VARCHAR(18) PRIMARY KEY,
    description VARCHAR(40),
    cost DECIMAL(10 , 2 ) NOT NULL CHECK (cost > 0),
    price DECIMAL(10 , 2 ) NOT NULL CHECK (price > 0),
    CHECK (price >= cost)
); 
```
一旦CHECK约束到位，无论何时插入或更新导致布尔表达式求值为false的值，数据库系统都会拒绝更改。

不幸的是，MySQL不支持CHECK约束。

MySQL的CREATE TABLE语句接受CHECK像SQL标准的声明CHECK语句。

但是，MySQL默默地忽略CHECK约束并且不执行数据验证。

可以使用触发器或视图实现CHECK约束.

使用触发器的MySQL CHECK约束

要CHECK在MySQL中模拟约束，可以使用两个触发器：BEFORE INSERT 和 BEFORE UPDATE。

实例：
```sql
CREATE TABLE IF NOT EXISTS parts (
    part_no VARCHAR(18) PRIMARY KEY,
    description VARCHAR(40),
    cost DECIMAL(10 , 2 ) NOT NULL,
    price DECIMAL(10,2) NOT NULL
); 
```
创建一个存储过程以检查cost和price列中的值。
```sql
DELIMITER $
 
CREATE PROCEDURE `check_parts`(IN cost DECIMAL(10,2), IN price DECIMAL(10,2))
BEGIN
    IF cost < 0 THEN
        SIGNAL SQLSTATE '45000'
           SET MESSAGE_TEXT = 'check constraint on parts.cost failed';
    END IF;
    
    IF price < 0 THEN
 SIGNAL SQLSTATE '45001'
    SET MESSAGE_TEXT = 'check constraint on parts.price failed';
    END IF;
    
    IF price < cost THEN
 SIGNAL SQLSTATE '45002'
           SET MESSAGE_TEXT = 'check constraint on parts.price & parts.cost failed';
    END IF;
END$
DELIMITER ; 
```
创建 BEFORE INSERT 并 BEFORE UPDATE 触发。在触发器内部，调用check_parts()存储过程。

```sql
-- before insert
DELIMITER $
CREATE TRIGGER `parts_before_insert` BEFORE INSERT ON `parts`
FOR EACH ROW
BEGIN
    CALL check_parts(new.cost,new.price);
END$   
DELIMITER ; 
-- before update
DELIMITER $
CREATE TRIGGER `parts_before_update` BEFORE UPDATE ON `parts`
FOR EACH ROW
BEGIN
    CALL check_parts(new.cost,new.price);
END$   
DELIMITER ; 
```

### 使用Views检查MySQL CHECK约束
创建一个带有基表检查选项的视图。在SELECT视图的语句中，只选择满足CHECK条件的有效行。

如果对视图进行任何插入或更新，将导致新行不显示在视图中。

```sql
DROP TABLE IF EXISTS parts;
 
CREATE TABLE IF NOT EXISTS parts_data (
    part_no VARCHAR(18) PRIMARY KEY,
    description VARCHAR(40),
    cost DECIMAL(10 , 2 ) NOT NULL,
    price DECIMAL(10,2) NOT NULL
); 
```
创建parts基于parts_data表命名的视图。
这样可以保持使用parts表的应用程序的代码保持不变。
旧parts表的所有权限保持不变。
```sql
CREATE VIEW parts AS
    SELECT 
        part_no, description, cost, price
    FROM
        parts_data
    WHERE
        cost > 0 AND price > 0 AND price >= cost 
WITH CHECK OPTION; 
```
通过parts视图在parts_data表中插入一个新行：
```sqll
INSERT INTO parts(part_no, description,cost,price)
VALUES('A-001','Cooler',100,120); 
```
尝试插入一个不会出现在视图中的新行。
