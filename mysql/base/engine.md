# MySQL 存储引擎

MySQL为其表提供各种存储引擎：

- MyISAM数据
- InnoDB的
- 合并
- 记忆（HEAP）
- ARCHIVE
- CSV
- FEDERATED

每个存储引擎都有自己的优点和缺点。<br/>
了解每个存储引擎功能并为表选择最合适的功能以最大限度地提高数据库性能至关重要。


### MyISAM 引擎
MyISAM扩展了以前的ISAM存储引擎。MyISAM表针对压缩和速度进行了优化。MyISAM表也可以在平台和操作系统之间移植。

- MyISAM表的大小可以达到256TB，这是巨大的。
- MyISAM表可以压缩为只读表以节省空间。
- MySQL会在启动时检查MyISAM表是否存在损坏, 甚至在出现错误时对其进行修复。
- MyISAM表不是事务安全的。

在MySQL 5.5之前，当您创建表而未明确指定存储引擎时，MyISAM是默认存储引擎。
从版本5.5开始，MySQL使用InnoDB作为默认存储引擎。

### InnoDB 引擎

与MyISAM一样，InnoDB表可在不同平台和操作系统之间移植。必要MySQL还会在启动时检查和修复InnoDB表。

- InnoDB表完全支持符合ACID和事务, 它们也是性能的最佳选择。
- InnoDB表支持外键，提交，回滚，前滚操作。
- InnoDB表的大小最高可达64TB。


### MERGE 引擎
MERGE表是一个虚拟表，它将多个MyISAM表组合在一起，这些表具有与一个表类似的结构。MERGE存储引擎也称为MRG_MyISAM引擎。MERGE表没有自己的索引; 它使用组件表的索引。

使用MERGE表，可以在连接多个表时加快性能  。MySQL只允许您对MERGE表执行SELECT，DELETE，UPDATE和INSERT操作。如果DROP TABLE在MERGE表上使用MERGE语句，则仅删除规范。基础表不会受到影响。

### Memory 引擎
内存表存储在内存中并使用哈希索引，因此它们比MyISAM表更快。内存表数据的生命周期取决于数据库服务器的正常运行时间。内存存储引擎以前称为HEAP。

### Archive 引擎
归档存储引擎允许您将大量记录（用于归档）存储为压缩格式以节省磁盘空间。存档存储引擎在插入时压缩记录，并在读取时使用zlib库对其进行解压缩。

归档表仅允许INSERT和SELECT语句。ARCHIVE表不支持索引，因此需要对表读取行进行全表扫描。

### CSV
CSV存储引擎以逗号分隔值（CSV）文件格式存储数据。CSV表提供了一种将数据迁移到非SQL应用程序（如电子表格软件）的便捷方法。

CSV表不支持NULL数据类型。此外，读取操作需要全表扫描。

### FEDERATED
FEDERATED存储引擎可让您无需使用群集或复制技术管理从远程MySQL服务器的数据。本地联合表不存储任何数据。从本地联合表查询数据时，将从远程联合表中自动提取数据。

