# 导入/出 CSV


### 导出

CSV代表逗号分隔值。CSV文件经常用在Microsoft Excel，Open Office，Google Docs等之间交换数据。

以CSV文件格式从MySQL数据库获取数据将非常有用，可以按照自己的方式分析和格式化数据。

MySQL提供了一种将查询结果导出到驻留在数据库服务器中的CSV文件的简便方法。

注意事项：
- MySQL服务器的进程具有对包含目标CSV文件的目标文件夹的写访问权。
- CSV文件必须不存在。

实例：
```sql
SELECT 
    orderNumber, status, orderDate, requiredDate, comments
FROM
    orders
WHERE
    status = 'Cancelled'; 

-- 导出语句
SELECT 
    orderNumber, status, orderDate, requiredDate, comments
FROM
    orders
WHERE
    status = 'Cancelled' 
INTO OUTFILE 'C:/tmp/cancelled_orders.csv' 
FIELDS ENCLOSED BY '"' 
TERMINATED BY ';' 
ESCAPED BY '"' 
LINES TERMINATED BY '\r\n'; 
```
上面代码创建了一个CSV文件cancelled_orders.csv。

- CSV文件，每一行由一系列回车符和LINES TERMINATED BY '\r\n'子句指定的换行符终止。
- 每行包含结果集中行的每列的值。
- 每个值用FIELDS ENCLOSED BY '”'双引号括起来。这防止包含逗号','的值被解释为字段分隔符。
- 使用双引号时，逗号不会被识别为字段分隔符。


##### 将数据导出到文件名为 timestamp的CSV文件

经常需要将数据导出到CSV文件中，文件的名称包含创建文件的时间戳。为此，需要使用MySQL预处理语句。

以下命令将整个订单表导出为CSV文件，其中时间戳作为文件名的一部分。
```sql
SET @TS = DATE_FORMAT(NOW(),'_%Y_%m_%d_%H_%i_%s');
 
SET @FOLDER = 'c:/tmp/';
SET @PREFIX = 'orders';
SET @EXT    = '.csv';
 
SET @CMD = CONCAT("SELECT * FROM orders INTO OUTFILE '",@FOLDER,@PREFIX,@TS,@EXT,
    "' FIELDS ENCLOSED BY '\"' TERMINATED BY ';' ESCAPED BY '\"'",
    "  LINES TERMINATED BY '\r\n';");
 
PREPARE statement FROM @CMD;
 
EXECUTE statement; 
```
解释

- 构造一个查询，其中当前时间戳作为文件名的一部分。
- 使用PREPARE语句 准备了执行语句。
- 使用EXECUTE命令执行了语句。
- 您可以通过事件包装命令，并在需要时定期运行事件。


##### 使用列标题导出数据
如果CSV文件包含第一行作为列标题将是方便的，这样文件更容易理解。

要添加列标题，需要使用UNION语句：
```sql
(SELECT 'Order Number','Order Date','Status')
UNION 
(SELECT orderNumber,orderDate, status
FROM orders
INTO OUTFILE 'C:/tmp/orders.csv'
FIELDS ENCLOSED BY '"' TERMINATED BY ';' ESCAPED BY '"'
LINES TERMINATED BY '\r\n'); 
```


##### 处理NULL值
如果结果集中的值包含NULL值，则目标文件将包含  "N而不是NULL。
要解决此问题，需要使用IFNULL函数将NULL值替换为另一个值，例如，不适用（N/A）：
```sql
SELECT orderNumber, orderDate, IFNULL(shippedDate, 'N/A')
FROM
    orders INTO OUTFILE 'C:/tmp/orders2.csv' 
    FIELDS ENCLOSED BY '"' 
    TERMINATED BY ';' 
    ESCAPED BY '"' LINES 
    TERMINATED BY '\r\n'; 
```
用N/A字符串替换shippedDate列中的 NULL值。CSV文件显示 N/A 而不是NULL值。


### 导入

LOAD DATA INFILE语句允许您从文本文件中读取数据，并将文件的数据快速导入数据库表。

规则：
- CSV文件中的数据与表的列数和每列中的数据类型相匹配。
- 帐户具有FILE和INSERT权限。

实例：
```sql
CREATE TABLE discounts (
    id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL,
    expired_date DATE NOT NULL,
    amount DECIMAL(10 , 2 ) NULL,
    PRIMARY KEY (id)
); 
```
下面是discounts.csv文件包含的数据。
```
id,title,expired_date,amount
1,"Spring Break 2014",20140410,20
2,"Back to School 2014",20140901,25
3,"Summer 2014",20140825,10
```

倒入语句
```sql
LOAD DATA INFILE '/Users/rick/Downloads/discounts.csv' 
INTO TABLE discounts 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 ROWS; 
```
FIELD TERMINATED BY ',' 字段用, 分割
ENCLOSED BY '"' 字段的双引号括起来。
LINES TERMINATED BY '\n' 每一行都由指示的换行符终止 

文件列标题在第一行，不应将其导入表中，通过 IGNORE 1 ROWS 忽略

如果出错：
```
ERROR 1290 (HY000): The MySQL server is running with the --secure-file-priv option so it cannot execute this statement
```
可以使用命令查看配置：
```sql
mysql> show variables like '%secure%';

+--------------------------+-------+
| Variable_name            | Value |
+--------------------------+-------+
| require_secure_transport | OFF   |
| secure_auth              | ON    |
| secure_file_priv         | NULL  |
+--------------------------+-------+
```
这里secure_file_priv参数的值是null，说明mysql中就没有设置secure_file_priv这个选项。

如果这个参数是个特定的文件路径，就说明文件只有在这个路径下才能将它导入导出到mysql。

参数secure_file_priv的值及含义如下：

| - secure_file_priv值 |	  含义  |
| :---  			   |  ---: |
| NULL  |	禁止文件的导入导出 |
| ‘’	（空字符串）| 允许所有文件的导入导出 |
| 一个特定的路径地址 |	只有该路径地址下的文件可以导入导出到mysql |

这个问题可以通过修改 my.cnf 配置文件：
```
#写在 my.ini 配置文件中
#允许导入文件
secure_file_priv=''
```
重启数据库即可


##### 导入时转换数据

有时数据的格式与表中的目标列不匹配。通常情况下，可以使用SET语句中的子句 对其进行转换

假设  discount_2.csv文件的到期日期列是  mm/dd/yyyy格式。
```
id,title,expired_date,amount
1,"Spring Break 2014",1/4/2014,20
2,"Back to School 2014",1/9/2014,25
3,"Summer 2014",25/8/2014,10
```
将数据导入discounts表时，我们使用str_to_date() 函数将其转换为MySQL日期格式，如下所示：
```sql
LOAD DATA INFILE 'c:/tmp/discounts_2.csv'
INTO TABLE discounts
FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(title,@expired_date,amount)
SET expired_date = STR_TO_DATE(@expired_date, '%m/%d/%Y'); 
```

##### 将文件从客户端导入到远程MySQL数据库服务器

可以使用LOAD DATA INFILE语句将数据从客户端（本地计算机）导入到远程MySQL数据库服务器。

当使用 LOCAL选项时 LOAD DATA INFILE，程序将读取客户端上的文件将其发送到MySQL服务器。

文件将加载到数据库服务器操作系统的临时文件夹中，

例：在Windows上的C:\windows\temp 或 Linux上/temp，文件夹不可由MySQL配置或确定。

实例：
```sql
LOAD DATA LOCAL INFILE  'c:/tmp/discounts.csv'
INTO TABLE discounts
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS; 
```
区别是LOCAL声明中的选项。

如果加载一个大的CSV文件，将使用LOCAL选项，加载文件会慢一点，因为将文件传输到数据库服务器需要一些时间。

使用LOCAL选项时，连接到MySQL服务器的帐户不需要具有FILE权限即可导入文件。

应注意将文件从客户端导入到远程数据库服务器潜在的安全风险。

