# Mysql 语句

Mysql 中常用的语句表达式

### 表达式
使用SELECT * FROM 表名 可以查询到一张表的所有记录。<br/>

很多时候，我们并不希望获得所有记录，而是根据条件选择性地获取指定条件的记录,
SELECT语句可以通过WHERE条件来设定查询条件，查询结果是满足查询条件的记录。

 `SELECT * FROM 表名 WHERE 条件表达式`
``` sql
SELECT * FROM user WHERE age >= 80;
``` 
### **1.And条件表达式** 
- `<条件1> AND <条件2>` 满足条件1 `并且` 满足条件2

``` sql
SELECT * FROM user WHERE age >= 80 AND tag = 'M';
```

### **2.Or条件表达式** 
- `<条件1> OR <条件2>` 满足条件1 `或者` 满足条件2

``` sql
SELECT * FROM user WHERE age >= 80 OR tag = 'M';
```

### **3.NOT条件表达式** 
- `NOT <条件>` 表达 `不符合该条件` 的记录

``` sql
SELECT * FROM user WHERE NOT id = 2;
```

### **4.组合条件表达式** 
- `NOT`, `OR`, `AND` 二种及以上组合使用

``` sql
SELECT * FROM user WHERE (age < 80 OR age > 90) AND tag = 'M';
```
**注意：**
- 加上括号可以改变优先级。
- 不加括号，按NOT、AND、OR的优先级进行执行，`NOT > AND > OR`。

#### **`常用的条件表达式`**

| 条件   | 表达式举例1 | 表达式举例2 | 说明   |
| :------- | :--------------: | :--------------: | :------- |
| 使用`=`判断相等 | score = 80	| name = 'abac' | 字符串需要用单引号括起来 |
| 使用`>`判断大于	| score > 80 | name > 'abc'	| 字符串比较根据ASCII码，中文字符比较根据数据库设置 |
| 使用`<`判断小于	| score < 80 | name <= 'abc' |   |
| 使用`>=`判断大于或相等 | score >= 80 | name >= 'abc' |    |
| 使用`<=`判断小于或相等 | score <= 80 | name <= 'abc' |	  |
| 使用`!=`判断不等于	| score != 80 | name != 'abc' |   |
| 使用`!<`判断不小于	| score !< 80 | age !< 18 |   |
| 使用`!>`判断不大于	| score !> 80 | age !> 100 |   |
| 使用`<>`判断不相等 | score <> 80 | name <> 'abc' |    |
| 使用`LIKE`判断相似 | name LIKE 'ab%' | name LIKE '_bc%'	| %表示任意字符，例如'ab%'将匹配'ab'，'abc'，'abcd' |
| 使用`_`单字通配符 | name LIKE 'a_b' | name LIKE '__b' | _表示任意`1个不为空`的字符,如 aob,a-b, 不能是ab |
| 使用`%`多字通配符，长度任意 | name LIKE 'a%b' | name LIKE '%b%' | %表示任意`多个字符`,如 aooob,aasdab |
| 使用`[]`集合通配符 | name LIKE '[ab]%' | name LIKE '%[bsd]_' | []表示集合中的任意一个字符,如 aooob,basdsb |
| 使用`^`集合取反 | name LIKE '[^ab]%' | name LIKE '%[^bsd]_' | []表示不是集合中的任意一个字符,如 cooob,dasdab |
|  |  |  |  |

- **注:** 单字符匹配和多字符匹配还可以一起使用, 如: `%n_`

<br />

### 通配符

1. **使用`_`单字通配符** 
- `_` 表示任意`1个非空字符`
``` sql
/** 结尾为e的字符,ze, ae, 不能是 e,ec,ase */
SELECT * FROM students WHERE name LIKE '_e'; 
/** 可以用 连续2个_ 表示2个字符 */
SELECT * FROM students WHERE name LIKE 'e__'; 
```

2. **使用`%`多字通配符，长度任意**
- `%` 表示任意 `多个字符`
``` sql
/** 结尾为e的任意长字符,ze, abcde */
SELECT * FROM students WHERE name LIKE '%e'; 
/** 名字中包含 m 的学生 */
SELECT * FROM students WHERE name LIKE '%m%'; 
/** 名字中以 m 字符，为倒数第二的学生 */
SELECT * FROM students WHERE name LIKE '%m_'; 
```

3. **使用`[]`集合通配符** 
- `[]` 表示集合中的任意一个字符
``` sql
/** 结尾为e,w,a 中的任意长字符,ze, abcda,asdaw */
SELECT * FROM students WHERE name LIKE '%[ewa]'; 
```

4. **使用`^`集合取反**
- `^` 表示 `不是集合中` 的任意一个字符
``` sql
/** 结尾为不是e,w,a 中的任意长字符,zn, abcdc,asdas */
SELECT * FROM students WHERE name LIKE '%[^ewa]'; 
```
> **注:** 单字符匹配和多字符匹配还可以一起使用,如: `%n_`

5. **多值检测**  - `IN (值1,值2,值3......)`
要将年龄为20岁、25岁和30岁的员工检索出来
``` sql
/** 可以使用 OR 的写法 */
SELECT * FROM students WHERE age=20 OR age=25 OR age=30;
/** 以下是 IN 的写法 */
SELECT * FROM students WHERE age IN (20,25,30);
```

6. **范围值检测**  - `字段名 BETTWEEN 左范围值 AND 右范围值`
检索年龄介于20岁到27岁之间的员工

``` sql
/** 以下是 AND 的写法 */
SELECT * FROM students WHERE age>=20 AND age=<27;
/** 以下是 BETTWEEN AND 的写法 */
SELECT * FROM students WHERE age BETTWEEN 20 AND 27;
```
7. **空值检测**

没有添加非空约束列是可以为`空值`的(也就是 `NULL`)，有时我们需要对空值进行检测，比如要查询所有姓名未知的员工信息。

在SQL语句中对空值的处理有些特别，`不能使用`普通的`等于运算符`进行判断，而要使用`IS NULL`关键字。
``` sql
SELECT * FROM students WHERE name IS NULL;

/** 如果要检测“字段不为空” */
SELECT * FROM students WHERE name IS NOT NULL;
```

8. **`低效的“WHERE 1=1”`**

1.经常会看到有人写 `WHERE 1=1` 这样的SQL是什么意思?

要求提供一个按四个查询条件，包括`工号`、`姓名`、`年龄`的条件来查询员工信息，每个`查询条件`都是一个`复选框`，如果选中，表示做为一个过滤条件。

这时过滤条件会`随着`用户的`选择而不同`，这时就要根据用户的设置来`动态组装SQL`。

要实现动态的SQL语句拼装，可以建立一个字符串，然后逐个判断复选框`是否选中`，向字符串中`添加对应SQL语句`。

这里有个问题当复选框被`选中`的时候SQL语句是`含有WHERE`子句，而当所有的复选框`都没选中`时就`没有WHERE`子句，因此在`添加每个过滤条件`时都要判断`是否已存在WHERE语句`，如果`没有WHERE`语句`则添加WHERE`语句。

在判断`每个复选框`的时候`都要`去判断，这使用起来非常麻烦，“聪明会偷懒的程序员”想到了一个`捷径`为SQL语句`指定一个`永远`为真`的条件语句(比如`1=1`)，这样就`不用考虑`WHERE语句`是否存在`的问题了。

这样看似`优美`的`解决问题`了，殊不知这样`很可能会`造成`非常大的性能损失`，因为使用添加`“1=1”`的过滤条件以后数据库系统就`无法使用索引`等查询`优化策略`，数据库系统将被迫`对每行数据进行扫描`(也就是全表扫描)以比较此行是否满足过滤条件，当表中`数据量比较大`的时候查询速度会`非常慢`。


### 列与别名

使用SELECT * FROM <表名> WHERE <条件>可以选出表中的若干条记录。

此时返回的二维表结构和原表是相同的，即所有列与原表的都一一对应。

1. **指定列** 
如果只需要展示部分列的数据, 可以用`SELECT 列1, 列2, 列3 FROM ...`, 查询指定列, 这种操作称为投影查询。

> 注：其中的星号 `*` 就意味着 `所有列`，星号`*`可以理解为一种简写。
``` sql
SELECT id, name, score FROM students;
```

2. **列别名** 
由于编码命名规范、编程框架要求等的限制，数据表的列名有的时候意思并不是非常易读, 这时可以使用别名来引用列, 格式为`列名 AS 别名`。

> SELECT 列名 AS 别名, 列名2 AS 别名2 FROM students;

``` sql
SELECT uage AS age, fname AS name FROM students;
```

定义别名的时候“AS”不是必须的，是可以省略的，比如下面的 SQL 也是正确的
> SELECT 列名 别名, 列名2 别名2 FROM students;

```sql
SELECT uage age, uname name FROM students;
```


### 数据汇总

有时需要对数据库中的数据进行一些统计，比如统计员工总数、统计年龄大于25岁的员工中的最低工资、统计工资大于3800元的员工的平均年龄。

SQL中提供了聚合函数来完成计算统计结果集条数、某个字段的最大值、某个字段的最小值、某个字段的平均值以及某个字段的合计值等数据统计的功能，SQL标准中规定了下面几种聚合函数。

| 函数名  | 说明  |
| :----- | :----- |
| MAX | 计算字段最大值 |
| MIN | 计算字段最小值 |
| AVG | 计算字段平均值 |
| SUM | 计算字段总计值 |
| COUNT | 统计数据条数 |

1. **使用**
``` sql
/** 计算字段最大值 */
SELECT MAX(score) FROM students WHERE course='数学';
/** 为了方便查看结果，可以为聚合函数的计算结果指定一个别名 */
SELECT MAX(score) AS max_score FROM students WHERE course='数学';

/** 计算字段最小值 */
SELECT MIN(score) AS min_score FROM students WHERE course='数学';

/** 计算字段平均值 */
SELECT AVG(score) AS avg_score FROM students WHERE course='数学';

/** 计算字段总计值 */
SELECT SUM(score) AS sum_score FROM students WHERE course='数学';

/** 统计数据总数 */
SELECT COUNT(*) AS total FROM students;
SELECT COUNT(score) AS total FROM students;

/** 组合使用 */
SELECT MIN(score) min,MAX(score) max,AVG(score) score FROM students WHERE course='数学
```

> `COUNT(*)`、`COUNT(score)` 都能统计出数据的条数，不少人认为这两种方式是等价的。

#### **区别:**
- `COUNT(*)` 统计的是结果集的总条数，而 `COUNT(列1)` 统计的则是除了结果集中 `列1` 不为空值(也就是不等于 NULL)的记录的总条数。
- 在使用 `COUNT` 的时候 **注意** 要区分两种使用方式的区别。




### 计算字段

有时候需要的数据不是数据表中的，必须经过一定的计算、转换或者格式化，这种情况下我们可以在宿主语言中通过编 写代码的方式来进行这些计算、转换或者格式化的工作，但是可以想象当数据量比较大的时候这样处理的速度是非常慢的。

计算字段是数据库系统提供的对数据进行计算、转换或者格式化的功能，由于是在数据库系统内部进行的这些工作，而且数据库系统都这些工作进行了优化，所以其处理效率比在宿主语言中通过编写代码的方式进行处理要高效的多。

1. `常量字段`
``` sql 
SELECT 'xxx学校',grade,name,age FROM students;
```
这里的'xxx学校'并不是实际的存在的列，但在查询出来的数据中，它们看起来是实际存在，这称为“常量字段”(也称为“常量值”)，它们可以被看成一个确定值的字段，比如可以为常量字段指定别名。
``` sql 
SELECT 'xxx学校' as school,grade,name,age FROM students;
```

2. `字段间计算`
``` sql
SELECT grade,name,score/courseCount as avgScore FROM students;
```
- `计算字段`也可以在`WHERE`语句等子句或者`UPDATE`、`DELETE`中使用。
``` sql
SELECT * FROM students WHERE score/courseCount > 70;
```

3. **数据处理函数**
像编程语言一样，SQL也支持使用函数处理数据，函数使用若干字段名或者常量值做为参数;参数的数量是不固定的，有的函数的参数为空，甚至有的函数的参数个数可变; 几乎所有函数都有返回值，返回值即为函数的数据处理结果。

最典型的就是“聚合函数”，“聚合函数”是函数的一种，它们可以对一组数据进行统计计算。除了“聚合函数”，SQL中还有其他类型的函数，比如进行数值处理的数学函数、进行日期处理的日期函数、进行字符串处理的字符串函数等。

- 主流数据库系统都提供了计算字符串长度的函数 `LENGTH(字符串)`
- 取得字符串的子串的函数 `SUBSTRING`有三个参数，字符串，起始位置(从1开始)，字串的长度；
- 多个函数可以嵌套使用。主流数据库都提供了计算正弦值函数SIN和绝对值函数ABS，都接受一个数值类型的参数。
``` sql
SELECT name, LENGTH(name) AS nameLength FROM students WHERE name IS NOT NULL;

SELECT name, SUBSTRING(name,2,3) FROM students WHERE name IS NOT NULL;

SELECT name,age, SIN(age) , ABS(SIN(age)) FROM students;
```

4. **字符串的拼接**
SQL允许`两个`或者`多个字段`之间进行`计算`，`字符串`类型的字段也`不例外`。比如需要以“工号+姓名”的方式显示员工的信息，就需要把工号和姓名两字段拼接计算; 再如需要在每个员工的工号前增加“Code”。这时候就需要对字符串类型的字段(包括字符串类型的常量字段)进行拼接。

- 所有的数据库系统都支持用`单引号`包围的形式定义的字符串，建议使用`单引号包围`的形式定义的字符串,可以使用加号 `+` 来连接两个字符串;

``` sql
SELECT 'Code'+id,name,age FROM students;
```
- 当用加号 `+` 连接两个字段的时候，`MYSQL会`尝试`将字段`值转换为`数字类型`(如果转换`失败`则认为字段值`为0`)，然后进行字段的`加法运算`。因此当计算的`'12'+'33'`的时候，会将“12”和“33”转换为`数字类型`的12和33，然后计算`12+33`的值，结果就是=45了。

- 在MYSQL中进行字符串的拼接要使用 `CONCAT` 函数，`CONCAT`函数支持`一个或者多个`参数，参数类型`可以`为字符串类型`也可以`是非字符串类型，对于`非字符串`类型的参将其`转化为`字符串类型，`CONCAT`函数会将所有参数`按照`参数的`顺序`拼接成`一个字符串`做为返回值。
``` sql
SELECT CONCAT('学号:',id,'年龄:',age) FROM students;
```
-` CONCAT`支持`只有一个`参数的用法，这时的CONCAT可以看作是一个将这个参数值尝试转化为字符串类型值的函数。
- `CONCAT_WS`，可以在待拼接的`字符串之间加入`指定的`分隔符`，它的第一个参数值是采用的分隔符，而剩下的参数则为待拼接的字符串值。
``` sql
SELECT CONCAT_WS(',',id,name,age) FROM students;
```

5. **计算字段的其他用途**
不仅能在`SELECT`语句中使用计算字段，同样`可以`在进行`数据过滤`、`数据删除`以及`数据更新`的时候使用计算字段。

```sql
/** 这里在 BETWEEN..AND.. 中使用了表达式 */
SELECT * FROM students WHERE score BETWEEN age*3 AND age*4; 

SELECT MAX(score/age) AS max_sa,MIN(score/age) AS min_sa FROM students;

/** 每过一年 年龄增加1 */
UPDATE students SET age=age+1;
```


### 排序

有时候我们需要按照某种排序规则来排列检索结果，比如按照年龄从高到低的顺序排列或者按照姓名的字符顺序排列等。

`SELECT` 语句允许使用 `ORDER BY` 子句来执行结果集的排序方式。

`ORDER BY` 子句位于 `SELECT` 语句的末尾，它允许指定按照`一个列`或者`多个列`进行排序，还可以指定排序方式是`升序`(从小到大排列)还是`降序`(从大到小排列)。

> `注:` ORDER BY 子句要放到 WHERE 子句`之后`，不能颠倒它们的顺序。

- `ASC` 升序. 默认的排序方式可以省略；
- `DESC` 降序。
- `ORDER BY` 语句允许指定`多个排序列`，列之间使用`逗号隔开`

> **注:** 多个排序规则，数据库系统会按照优先级进行处理。<br/>
首先按 `第一个排序` 规则进行排序;<br/>
如果第一个排序规则无法区分顺序，则按第二个排序规则排序;<br/>
如果第二个排序规则无法区分顺序，则按第三个排序规则排序;<br/>
...... 以此类推。

``` sql
/** 按照年龄生序 */
SELECT * FROM students WHERE score>60 ORDER BY age ASC;
/** 生序 可以省略 ASC */
SELECT * FROM students ORDER BY age;

/** 按照年龄降序 */
SELECT * FROM students ORDER BY age DESC

```

1. **限制结果集行数**
在进行数据检索的时候有时候需要只检索结果集中的部分行，比如说“检索排名前10的数据”，这种功能被称为“限制结果集行数”。
> `LIMIT`关键字用来限制返回的结果集;
``` sql
/** 降序排列 */
SELECT * FROM students ORDER BY score DESC LIMIT 0,9;
/** 生序排序 */
SELECT * FROM students ORDER BY score LIMIT 0,9;
```
2. **数据库分页**

在进行信息检索的时候，数量通常会很多，达到成千上万条，这不仅查看起来非常耗时，且过多的数据显示在界面上也会造成内存占用过多。解决这个问题的最常用方案就是分页，一个页面只显示50条，提供 上一页/下一页 等按钮用来显示不同的页。实现数据库分页的核心技术就是“限制结果集行数”。

- 分页实际上就是从结果集中截取出第 M~N 条记录。这个查询可以通过`LIMIT <M> OFFSET <N>`子句实现
- `LIMIT`  总是设定为`pageSize`；
- `OFFSET` 计算公式为`pageSize * (pageIndex - 1)`
``` sql
SELECT * FROM students ORDER BY score LIMIT s OFFSET i;
/** pageIndex, pageSize, LIMIT 后面可以写成: i=(pageIndex -1)*pageSize,s=pageIndex*pageSize */
/** 简写 */
SELECT * FROM students ORDER BY score LIMIT i,s;
```
> 注: 使用`LIMIT <M> OFFSET <N>`分页时，随着N越来越大，查询效率也会越来越低;

3. **抑制数据重复**
列出所有的班级的学科，不过很多学科名称是重复的，必须去掉这些重复的学科名称，每个重复学科只保留一个。`DISTINCT`关键字是用来抑制重复数据的最简单的方式，而且所有的数据库系统都支持`DISTINCT`，只要在 `SELECT之后`增加`DISTINCT`即可。

``` sql
SELECT DISTINCT course FROM students;
```
> `DISTINCT`是对`整个`结果集进行`数据重复抑制`的，而`不是`针对`每个列`:
``` sql
SELECT DISTINCT course,grade FROM students;
/** 检索结果中不存在 course 和 grade 列都重复的数据行，但是却存在 course 列重复的数据行，这就验证了“DISTINCT是对整个结果集进行数据重复抑制的” */
```


### 分组

数据分组用来将数据分为多个逻辑组，从而可以对每个组进行聚合运算。
SQL语句中使用 GROUP BY子句进行分组，使用方式为“GROUP BY 分组字段”。
分组语句必须和聚合函数一起使用，GROUP BY子句负责将数据分成逻辑组，而聚合函数则对每一个组进行统计计算。

虽然GROUP BY子句常常和聚合函数一起使用，不过GROUP BY子句并不是不能离开聚合函 数而单独使用的，虽然不使用聚合函数的GROUP BY子句看起来用处不大，不过它能够帮助我们 更好的理解数据分组的原理。

首先来看一下如果通过SQL语句实现“查看公司员工有哪些年龄段的”，因为这里只需要列出员工的年龄段。
``` sql
SELECT age FROM students ORDER BY age;
```
这个SQL语句查询表中的所有记录，并将 age 相同的数据行放到一组，可以把这些数据看作临时结果集。
- 需要分组的`所有列`都`必须`位于`GROUP BY`子句的`列名列表`中，也就是`没有`出现在`GROUP BY`子句中的列(聚合函数除外)是`不能`放到`SELECT`语句后的列名列表中的。
``` sql
SELECT age,score FROM students GROUP BY age; /** 这样是错误的 */
/** 因为采用分组的查询结果集是以分组形式提供的，由于每组学生成绩是不一样，所以就不存在能够代表本组水平的score值了，所以上面的SQL语句是错误的。但每组学生的平均分数却是能代表本组分数水平的，所以可以对score使用聚合函数 */
SELECT age,AVG(score) FROM students GROUP BY age; /** 正确的写法 */
```
- `GROUP BY`子句中可以指定多个列，`只需`将多个列的`列名`用`逗号隔开`即可。指定多个分组 规则以后，数据库系统将按照定义的分组顺序来对数据进行`逐层分组`，首`先按照`第一个分组，然后在每个`小组内`按照第`二个分组`列进行`再次分组`...逐层分组，从而实现“组中组” 的效果，而查询的`结果集`是以`最末一级`分组来进行`输出`的。
``` sql
/** 所有班级的学科情况 */
SELECT grade,course FROM students GROUP BY grade,course;
```
> 注: `GROUP BY`子句必须放到`SELECT`语句的`之后`，如果有`WHERE`语句，则必须放到WHERE`之后`。

1. **数据分组与聚合函数**
可以使用`聚合函数`来对`分组`后的数据进行统计，也就是`统计`每一个分组的`数据`。甚至可以认为在`没有`使用 `GROUP BY` 语句中使用`聚合函数`不过是对`整个`结果集，做为`一个分组`数据进行数据统计分析。
``` sql
/** 查看每个年龄段的的人数 */
SELECT age,COUNT(*) AS CountOfAge FROM students GROUP BY age;
/** 可以使用多个分组来实现更精细的数据统计 */

/** 统计每个班级的各年龄段的人数 */
SELECT grade,age,COUNT(*) AS CountOfGradeAge FROM students GROUP BY grade,age;
/** 上面默认是按照年龄进行排序的，统计每个班级的各年龄段的人数，按照班级排序 */
SELECT grade,age,COUNT(*) AS CountOfGradeAge FROM students GROUP BY grade,age GROUP BY grade;
```
> 注: `COUNT`、`SUM`、`AVG`、`MIN`、`MAX`都可以在分组中使用。

2. **HAVING 语句**
有的时候需要对部分分组进行过滤，比如只检索人数多余1的年龄段，有的开发人员会使
用下面的SQL语句:
``` sql
/** 这是错误的语句，因为聚合函数不能在WHERE语句中使用，必须使用HAVING子句来代替 */
SELECT age,COUNT(*) AS CountOfAge FROM students GROUP BY age WHERE COUNT(*)>1;
/** 正确写法 */
SELECT age,COUNT(*) AS CountOfAge FROM students GROUP BY age HAVING COUNT(*)>1;
```
> `HAVING`语句中也可以像`WHERE`语句一样使用`复杂`的过滤条件,`HAVING`语句能够使用的语法和`WHERE`几乎是`一样的`，不过使用`WHERE`的时候`GROUP BY`子句要`位于WHERE子句之后`，而使用`HAVING`子句的时候`GROUP BY`子句要`位于HAVING子句之前`;
``` sql
SELECT age,COUNT(*) AS CountOfAge FROM students GROUP BY age HAVING COUNT(*) IN (1,3);
```

> **特别注意:** 在`HAVING`语句中`不能`包含`未分组`的列名;
``` sql
/** 错误写法 */
SELECT age,COUNT(*) AS CountOfAge FROM students GROUP BY age HAVING name IS NOT NULL;
/** 正确写法 */
SELECT age,COUNT(*) AS CountOfAge FROM students WHERE name IS NOT NULL GROUP BY age;
```


### 函数

### 联表(聚合结果集)

有时候需要组合两个完全不同的查询结果集，而这两个查询结果之间没有必然的联系，只是需要将他们显示在一个结果集中。在SQL中可以使用 `UNION` 运算符来将两个或者多个查询结果集联合为一个结果集中。

``` sql
SELECT id,name,age FROM students
UNION
SELECT id,name,age FROM students2
```

- `UNION`可以连接多个结果集，就像`“+”`可以连接多个数字一样简单，只要在`每个结果集`之间加入UNION即可.
``` sql
SELECT id,name,age FROM students
UNION
SELECT id,name,age FROM students2
UNION
SELECT id,name,age FROM students3;
```
> 联合结果集的原则

联合结果集不必受被联合的多个结果集之间的关系限制，使用`UNION`仍然有两个基本的原则需要遵守
- 一是每个结果集必须有相同的列数;
- 二是每个结果集的列必须类型相容;

1. 第一个原则: 每个结果集必须有相同的列数，两个不同列数的结果集是不能联合在一起.
``` sql
/** 错误的写法 列不一致 */
SELECT code,name,age,grade FROM students
UNION
SELECT id,name,age FROM students2;

/** 正确的写法 */
SELECT code,name,age,grade FROM students
UNION
SELECT id,name,age,'未知年级' FROM students2;
```

2. 第二个原则是: 每个结果集的列类型必须相容，也就是每个对应列的数据类型必须相同或者能够转换为同一种数据类型。


#### `UNION ALL`
默认情况下`UNION`运算符合并了两个查询结果集，其中相同的数据行被合并为了一条。如果需要在联合结果集中返回所有的记录而不管它们是否唯一，则需要在 UNION运算符后使用ALL操作符。
``` sql
SELECT name,age FROM students
UNION ALL
SELECT name,age FROM students2;
```

1. **多表查询**
`SELECT`查询不但可以从`一张表`查询数据，`还可以`从`多张表同时`查询数据。

语法是：`SELECT * FROM <表1> <表2>`。
``` sql
SELECT * FROM students, classes;
```
这种一次查询两个表的数据，查询的结果会显示在一个二维表，它是students表和classes表的叠加，即students表的每一行与classes表的每一行都两两拼在一起返回。结果集的列数是students表和classes表的列数之和，行数是students表和classes表的行数之积。

这种多表查询又称笛卡尔查询，使用笛卡尔查询时要非常小心，由于结果集是目标表的行数乘积，对两个各自有100行记录的表进行笛卡尔查询将返回1万条记录，对两个各自有1万行记录的表进行笛卡尔查询将返回1亿条记录。

你可能还注意到了，上述查询的结果集有两列id和两列name，两列id是因为其中一列是students表的id，而另一列是classes表的id，但是在结果集中，不好区分。

要解决这个问题，我们仍然可以利用投影查询的“设置列的别名”来给两个表各自的id和name列起别名。
``` sql
SELECT
    students.id sid,
    students.name,
    students.score,
    classes.id cid,
    classes.name cname
FROM students, classes;
```

多表查询时要使用表名.列名这样的方式来引用列和设置别名，这样就避免了结果集的列名重复问题。但是用表名.列名这种方式列举两个表的所有列实在是很麻烦，所以SQL还允许给表设置一个别名。
``` sql
SELECT
    s.id sid,
    s.name,
    s.score,
    c.id cid,
    c.name cname
FROM students s, classes c;
/** 多表查询也是可以添加WHERE条件的 */
SELECT
    s.id sid,
    s.name,
    s.score,
    c.id cid,
    c.name cname
FROM students s, classes c
WHERE s.gender = 'M' AND c.id = 1;
```
`FROM`子句给表设置别名的语法是`FROM <表名1> <别名1>, <表名2> <别名2>`。这样我们用别名s和c分别表示students表和classes表。




### 连表(连接查询)

连接查询是另一种类型的多表查询。连接查询对多个表进行JOIN运算，简单地说，就是先确定一个主表作为结果集，然后，把其他表的行有选择性地“连接”在主表结果集上。

- `注意: 使用别名不是必须的，但可以更好地简化查询语句`

> **内连接（`INNER JOIN`)**;
``` sql
SELECT s.id, s.name, s.class_id, cla.name class_name, s.score
FROM students s
INNER JOIN classes cla
ON s.class_id = cla.id;
```
1. `INNER JOIN`查询的写法：
- 先确定主表，仍然使用FROM <表1>的语法；
- 再确定需要连接的表，使用INNER JOIN <表2>的语法；
- 然后确定连接条件，使用ON <条件...>，这里是s.class_id = c.id，表示students表的class_id列与classes表的id列相同的行需要连接；
- 可选：加上`WHERE`子句、`ORDER BY`等子句。

> **外连接（`OUTER JOIN`)**;
``` sql
SELECT s.id, s.name, s.class_id, c.name class_name, s.score
FROM students s
RIGHT OUTER JOIN classes c
ON s.class_id = c.id;
```
> 执行上述`RIGHT OUTER JOIN`可以看到，和`INNER JOIN`相比，`RIGHT OUTER JOIN`多了一行，多出来的一行是“四班”，但是，学生相关的列如name、gender、score都为NULL。
这也容易理解，因为根据`ON`条件s.class_id = c.id，`classes`表的id=4的行正是“四班”，但是，`students`表中并不存在class_id=4的行。

> 有`RIGHT OUTER JOIN`，就有`LEFT OUTER JOIN`，以及`FULL OUTER JOIN`。它们的区别是：

- `INNER JOIN` 返回同时存在于两张表的行。由于students表的class_id包含1，2，3，classes表的id包含1，2，3，4，所以，INNER JOIN根据条件s.class_id = c.id返回的结果集仅包含1，2，3。

- `RIGHT OUTER JOIN` 返回右表都存在的行。如果某一行仅在右表存在，那么结果集就会以NULL填充剩下的字段。

- `LEFT OUTER JOIN` 返回左表都存在的行。如果我们给students表增加一行，并添加class_id=5，由于classes表并不存在id=5的行，所以，LEFT OUTER JOIN的结果会增加一行，对应的class_name是NULL。

- `FULL OUTER JOIN` 返回两张表的所有行，并且自动把对方不存在的列填充为NULL。
