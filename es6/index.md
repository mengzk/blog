# ES6 入门教程

> 注：本文档**不可商用！！！**

> 本文档只作为个人学习使用，如有侵权请及时告知删除.

- [阮一峰文档地址](https://es6.ruanyifeng.com/)
- [阮一峰Github](https://github.com/ruanyf/es6tutorial/)


# 目录

- [ECMAScript 6简介](docs/intro.md)
- [let 和 const 命令](docs/let.md)
- [变量的解构赋值](docs/let.md)
- [字符串的扩展](docs/let.md)
- [字符串的新增方法](docs/let.md)
- [正则的扩展](docs/let.md)
- [数值的扩展](docs/let.md)
- [函数的扩展](docs/let.md)
- [数组的扩展](docs/let.md)
- [对象的扩展](docs/let.md)
- [对象的新增方法](docs/let.md)
- [运算符的扩展](docs/let.md)
- [Symbol](docs/let.md)
- [Set 和 Map 数据结构](docs/let.md)
- [Proxy](docs/let.md)
- [Reflect](docs/let.md)
- [Promise 对象](docs/let.md)
- [Iterator 和 for...of 循环](docs/let.md)
- [Generator 函数的语法](docs/let.md)
- [Generator 函数的异步应用](docs/let.md)
- [async 函数](docs/let.md)
- [Class 的基本语法](docs/let.md)
- [Class 的继承](docs/let.md)
- [Module 的语法](docs/let.md)
- [Module 的加载实现](docs/let.md)
- [编程风格](docs/let.md)
- [读懂规格](docs/let.md)
- [异步遍历器](docs/let.md)
- [ArrayBuffer](docs/let.md)
- [最新提案](docs/let.md)
- [Decorator](docs/let.md)
- [参考链接](docs/let.md)


## 版权许可

本书采用“保持署名—非商用”创意共享4.0许可证。

只要保持原作者署名和非商用，您可以自由地阅读、分享、修改本书。

详细的法律条文请参见[创意共享](http://creativecommons.org/licenses/by-nc/4.0/)网站。
